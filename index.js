const database = require('database');
const moment = require('moment');
const AWS = require('aws-sdk');

AWS.config.update({
  region: process.env.AWS_REGION,
  accessKeyId: process.env.AWS_KEYID,
  secretAccessKey: process.env.AWS_ACCESSKEY
});

const SQS = new AWS.SQS();
const PROVA_EVENT_TYPE = 'PROVA';

const QSearchMaxEventTypeDate = '\
SELECT \
  DATE_FORMAT(MAX(ec.start_date), \'%Y-%m-%dT%TZ\') as startDate \
FROM sp_student_rules sr \
  INNER JOIN sp_discipline_rules dr ON (sr.disciplineruleid = dr.id) \
  INNER JOIN sp_score_rules rules ON (dr.scoreruleid = rules.id) \
  INNER JOIN sp_event_config ec ON (rules.id = ec.ruleid) \
  INNER JOIN sp_event_type et ON (ec.eventid = et.id) \
WHERE \
  et.name = ? \
  AND sr.id = ? \
GROUP BY dr.id';

const sendToSQS = (data, queueUrl) => {
  return new Promise((resolve, reject) => {
    const params = {
      MessageBody: JSON.stringify(data),
      QueueUrl: queueUrl
    };

    SQS.sendMessage(params, (err) => {
      if (err) { return reject(err); }
      return resolve();
    });
  });
};

const getMaxEventTypeDate = (conn, studentRuleId) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchMaxEventTypeDate, [PROVA_EVENT_TYPE, studentRuleId], (err, result) => {
      if (err) { reject(err); }
      if (result.length) { resolve(result[0].startDate); }

      const message = `Unable to find max event date from event ${PROVA_EVENT_TYPE} and studentRuleId ${studentRuleId}`;
      return reject(new Error(message));
    });
  });
};

const isDateValid = (receivedDate, eventDate) => {
  const eventTime = moment(eventDate).utc();
  const receivedTime = moment.utc(receivedDate).milliseconds(999);
  return receivedTime.isSameOrAfter(eventTime);
};

/**
 * Indicates if the approval process should be started comparing the date of the last PROVA event and the received date
 */
const shouldStartApprovalProcess = (studentRuleId, receivedDate) => {
  return new Promise((resolve, reject) => {
    let conn = null;

    database.getConnection()
      .then((connection) => {
        conn = connection;
        return getMaxEventTypeDate(conn, studentRuleId);
      })
      .then((startApprovalDate) => {
        resolve(isDateValid(receivedDate, startApprovalDate));
      })
      .catch((err) => reject(err))
      .then(() => {
        if (conn) {
          conn.release();
        }
      });
  });
};

/**
 * Send message to Approval Queue if the approval process should be initiated
 */
const startApprovalProcess = (username, aluCode, studentRuleId, receivedDate) => {
  return new Promise((resolve, reject) => {
    let conn = null;

    database.getConnection()
      .then((connection) => {
        conn = connection;
        return getMaxEventTypeDate(conn, studentRuleId);
      })
      .then((startApprovalDate) => {
        const shouldStart = isDateValid(receivedDate, startApprovalDate);

        if (!shouldStart) {
          return Promise.resolve();
        }

        const student = { username, aluCode, studentRuleId };

        return sendToSQS(student, process.env.AWS_SQS_APPROVAL);
      })
      .then(() => {
        resolve();
      })
      .catch((err) => reject(err))
      .then(() => {
        if (conn) {
          conn.release();
        }
      });
  });
};

module.exports.shouldStartApprovalProcess = shouldStartApprovalProcess;
module.exports.startApprovalProcess = startApprovalProcess;
